import "bootstrap/dist/css/bootstrap.min.css";
import { Container } from "reactstrap";

import ContentComponent from "./components/content/ContentComponent";
import TitleComponent from "./components/title/TitleComponent";

function App() {
  return (
    <Container className="mt-5 text-center">
      <TitleComponent />
      <ContentComponent />
    </Container>
  );
}

export default App;
