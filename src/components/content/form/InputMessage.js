import { Button, Input, Label } from "reactstrap";

const InputMessage = ({inputMessageProps, inputMessageChangeHandlerProps, outputMessageChangeHandlerProps}) => {
    const inputChangeHandler = (event) => {
        let value = event.target.value;  
        inputMessageChangeHandlerProps(value);
    }
    const buttonClickHandler = () => {
        outputMessageChangeHandlerProps();
    }
    return (
        <>
            <div className="form-floating mt-5">
                <Input type="email" placeholder="Nhập message của bạn" onChange={inputChangeHandler} value={inputMessageProps}/>
                <Label>Message cho bạn 12 tháng tới</Label>
            </div>
            <Button color="success" className="m-5" onClick={buttonClickHandler}>Gửi thông điệp</Button>
        </>
    )
}

export default InputMessage;