import TitleImage from "./image/TitleImage";
import TitleText from "./text/TitleText";

const TitleComponent = () => {
    return (
        <>
            <TitleText />
            <TitleImage />
        </>
    )
}

export default TitleComponent;