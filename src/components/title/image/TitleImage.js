import background from "../../../assets/images/background.jpg";

const TitleImage = () => {
    return (
        <img className="img-thumbnail" src={background} alt="title background"/>
    )
}

export default TitleImage;